# Week 7 Linux Exercise

Rules : 
- Do any of the Exercise to get 60 points
- Do two of the Exercise to get 80 points
- Do three of the Exercise to get 100 points
- Contact me via discord to share your results

## Exercise 1 

1. Download this project by executing 

    `git clone https://gitlab.com/sgu-it-lab/comnet-week7.git`

2. Get into the directory by running 
    
    `cd comnet-week7`

3. Try to run the program start.js
    
    HINT : start.js is a javascript program, to run it, you need to install node.js or any other javascript client


## Exercise 2 
     
1. Download the file from the following url
 
    `https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt`

2. One option is to download using wget using the command
    
    HINT : Try using `wget`, if wget is not there in your virtual machine, try to install it

3. Using the command `grep`, attempt to find whether the following letter exist in the word list. `Abacinate, Superiment, Marieland`

## Exercise 3

1. Run the program `echo your_name` in your terminal
2. Show the history of your terminal
2. Put the history of the terminal in a file
    
    HINT : To put the result of a command to file, find a way to to use the command `>`
